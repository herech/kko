/*
 * Autor: Jan Herec (xherec00)
 * Datum: 11. února 2017
 * Soubor: ahed.cpp
 * Komentar: Zdrojový soubor knihovny ahed.c, která zajišťuje Huffmanovo adaptivní (de)kódování vstupního souboru. 
 *           Výsledek zapisuje do výstupního souboru.
 * Kodovani: UTF-8
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include "ahed.hpp"
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <new>   
#include <deque>
#include <math.h> 
#include <limits>

using namespace std;

// ##################### DEFINICE DATOVÝCH STRUKTUR ############################

#define NYT -2        // značí NYT (NOT YET TRANSMITTED) uzel
#define OLD_NYT -3    // značí starý NYT uzel
#define MAX_NODES 512 // max počet uzlů = 2n-1 kde n=256, tedy dohromady 511, ale 512 je hezci cislo

/**
 * Možné stavy při dekódování vstupu 
 */
enum DecodeStates { READ_9_BIT_CHARACTER,WRITE_DECODED_CHAR_TO_OUTPUT,READ_HUFFMAN_CODE};

/* Struktura, ktera představuje uzel huffmanova stromu */
typedef struct Node {
	int content;      // obsah uzlu (symbol, který reprezentuje)	
        Node *leftChild;  // levý potomek uzlu
        Node *rightChild; // pravý potomek uzlu
        Node *parent;     // rodič uzlu
        u_int64_t weight; // váha uzlu (frevence symbolu ve zprávě, pokud se jedná o listový uzel)
        int order;        // pořadí uzlu ve stromě
} Node;

/* Struktura, ktera obsahuje uzel stromu a jeho hloubku */
typedef struct DeepOfNode {
	int deep;   // hloubka uzlku ve stromu (0 = kořen, ...)
        Node *node; // uzel stromu
} DeepOfNode;

// ########################## PROTOTYPY FUNKCÍ #################################

/**
 * Funkce, která zjistí, jestli je daný uzel listem
 * @param node ukazatel na uzel huffmanova stromu
 * @return true pokud je uzel listem huffmanova stromu, jinak false
 */
bool isNodeLeaf(Node *node);

/**
 * Funkce, která zjistí, jestli je daný uzel kořenem
 * @param node ukazatel na uzel huffmanova stromu
 * @return true pokud je uzel kořenem huffmanova stromu, jinak false
 */
bool isNodeRoot(Node *node);

/**
 * Funkce ověří jestli zadaný uzel je rodič zadaného uzlu. 
 * Pozor rodič nemusí být přímý, ale i nepřímý = např. rodič rodiče uzlu apod. rodič je zde tedy jen terminus technikus. 
 * Vhodnější by možná bylo označení předchůdce, ale v literatuře se operuje s pojmem parent
 * @param potentialParentOfNode potenciální rodič uzlu node
 * @param node potenciální potomek uzlu potentialParentOfNode
 * @return true pokud je uzel potentialParentOfNode rodičem uzlu node, jinak false
 */
bool isNodeParent(Node *potentialParentOfNode, Node *node);

/**
 * Funkce vytvoří nový uzel, který se může dále začlenit do stromu
 * @param content obsah uzlu (symbol, který reprezentuje)
 * @param weight váha uzlu (frevence symbolu ve zprávě, pokud se jedná o listový uzel)
 * @param order pořadí uzlu ve stromě
 * @param parent rodič uzlu
 * @return ukazatel na nově vytvořený uzel
 */
Node * createNode(int content, u_int64_t weight, int order, Node *parent);

/**
 * Funkce vytvoří strom, tak že vytvoří uzel, který bude kořenem stromu
 * @return ukazatel na kořen stromu
 */
Node *createTree();

/**
 * Funkce dealokuje paměť kterou zabírá strom, až na jeho listy, které ponechá
 * @param tree ukazatel na kořen huffmanova stromu
 */
void deleteTreeExceptLeafs(Node* tree);

/**
 * Funkce najde a vrátí všechny listy v huffmanově stromu v pořadí zleva doprava
 * @param tree ukazatel na kořen huffmanova stromu
 * @return vektor ukazatelů na listy huffmnaova stromu
 */
vector<Node*> getAllLeafsInTree(Node* tree);

/**
 * Funkce provedete změnu měřítka (přeškálování) HUffmanova stromu, aby nedošlo k přetečení čítače váhy kořenového uzlu
 * @param tree ukazatel na kořen huffmanova stromu
 * @return ukazatel na kořen přeškálovaného HUffmanova stromu
 */
Node* scalingTree(Node* tree);

/**
 * Funkce nalezne a vrátí uzel, který obsahuje daný obsah (symbol), zároveň přes výstupní parametr vrátí cestu (huffmanův kód) k danému uzlu od kořenu
 * @param tree ukazatel na kořen huffmanova stromu
 * @param content hledaný obsah (symbol)
 * @param code výstupní parametr, pokud != NULL, potom do tohoto výstupního parametru uložíme kód (od ke kořenu k listu) hledaného symbolu v huffamnově stromu
 */
Node *searchNodeByContent(Node* tree, int content, deque<bool> *code);

/**
 * Funkce vykreslí (nebo spíše vypíše) strom (pro debuggování)
 * @param tree ukazatel na kořen huffmanova stromu
 * @param deep aktuální hloubka, ve které se nacházíme (0 = kořen, ...)
 */
void printTree(Node* tree, int deep);

/**
 * Funkce vypíše obsah uzlu huffmanova stromu
 * @param node ukazatel na uzel
 */
void printNode(Node *node);

/**
 * Funkce která nalezne a vrátí nejvýznamější (s největším agtributem order) uzel stromu v rámci daného bloku (uzly se stejným atributem weight)
 * @param tree ukazatel na kořen stromu
 * @param weight váha uzlů v daném bloku
 * @return ukazatel na nejvýznamnější uzel (s největším agtributem order)  v rámci daného bloku uzlů o stejné váze
 */
Node *getMostSignificantNodeOfGroup(Node *tree, u_int64_t weight);

/**
 * Funkce vrátí vektor obsahující struktury, kde každá struktura obsahuje uzel stromu a jeho hloubku
 * Jednotlivé uzly jsou seřazeny tak, že v rámci stejné hlozbky jsou ve vektoru seřazeny z leva do prava
 * Toho využijeme při přečíslování pořadí uzlů ve stromě pokud provedem swap uzlů
 * @param tree ukazatel na kořen huffmanova stromu
 * @param deep hloubka stromu ve které se pohybujeme
 * @return vektor obsahující struktury, kde každá struktura obsahuje uzel stromu a jeho hloubku
 */
vector<DeepOfNode> getListOfNodesAndTheirDeep(Node *tree, int deep);

/**
 * Funkce provede swap podstromů
 * @param tree ukazatel na kořen huffmanova stromu
 * @param leastSignificantNodeOfGroup ukazatel na nejméně významný uzel v rámci dané skupiny uzlů stejné váhy
 * @param mostSignificantNodeOfGroup ukazatel na nejvíce významný uzel v rámci dané skupiny uzlů stejné váhy
 */
void swapSubtrees(Node *tree, Node *leastSignificantNodeOfGroup, Node *mostSignificantNodeOfGroup);

/**
 * Funkce provede přečíslování pořadí uzlů ve stromu 
 * @param tree ukazatel na kořen huffmanova stromu
 */
void renumberOrderOfAllNodesInTree(Node *tree);

/**
 * Funkce provede update stromu (přidá uzel na dané msíto, nebo upraví váhu daného uzlu a případně provede swap podstromů a přečíslování pořadí uzlů)
 * @param tree ukazatel na kořen huffmanova stromu
 * @param content symbol, který chceme do stromu uložit (buď nově a nebo zvýšíme jho frekvenci, pokud už ve stromě existuje)
 * @return -1 pokud se update stromu nepodaří, jinak 0
 */
int updateTree(Node *tree, int content);

/**
 * Funkce zakóduje znak jako posloupnost (deque) bitů, s tím že jejich počet je 9
 * 1. bit totiž je příznak že se nejedná o pseudo EOF, tedy první bit musí být 1
 * @param inputChar vstupní symbol (bytový), který se zakóduje
 * @return fronta bitů, do kterých je vstupní symbol zakódován
 */
deque<bool> codeCharacter(int inputChar);

/**
 * Funkce je volána při dekódování kdy z přečteného bytu chceme získat posloupnost 0 a 1
 * Je docela podobná funkci codeCharacter, která se používá při kódování znaku na posloupnost bitů
 * @param inputChar vstupní symbol (bytový), ze kterého chceme získat posloupnost bitů
 * @return fronta bitů, ze kterých se vstupní symbol skládá
 */
deque<bool> getBitsSequenceFromByte(int inputByte);

/**
 * Funkce z posloupnosti bitů získá jeden bajt (z pprvních 8 bitů), který je možné zapsat do souboru
 * @param code fronta bitů (musí obsahovat alespoň 8 prvků/bitů)
 * @return bajt, který odpovídá dané posloupnosti bitů
 */
unsigned char getEncodedByteFromDequeOfBits(deque<bool> code);

/**
 * Funkce spojí dva datové kontejnery Deque/fronta obousměrná do jednoho (prvního)
 * @param firstDeque první fronta, ke které přidáme prvky z druhé fronty a tím obě fronty spojíme, jedná se o výstupní parametr
 * @param secondDeque druhá fronta
 */
template <class T>
void mergeDeques(deque<T> *firstDeque, deque<T> *secondDeque);

// ######################### DEFINICE FUNKCÍ ###################################

/**
 * Funkce, která zjistí, jestli je daný uzel listem
 * @param node ukazatel na uzel huffmanova stromu
 * @return true pokud je uzel listem huffmanova stromu, jinak false
 */
bool isNodeLeaf(Node *node) {
    if (node->leftChild == NULL && node->rightChild == NULL) {
        return true;
    }
    else {
        return false;
    }
}

/**
 * Funkce, která zjistí, jestli je daný uzel kořenem
 * @param node ukazatel na uzel huffmanova stromu
 * @return true pokud je uzel kořenem huffmanova stromu, jinak false
 */
bool isNodeRoot(Node *node) {
    if (node->parent == NULL) {
        return true;
    }
    else {
        return false;
    }
}


/**
 * Funkce ověří jestli zadaný uzel je rodič zadaného uzlu. 
 * Pozor rodič nemusí být přímý, ale i nepřímý = např. rodič rodiče uzlu apod. rodič je zde tedy jen terminus technikus. 
 * Vhodnější by možná bylo označení předchůdce, ale v literatuře se operuje s pojmem parent
 * @param potentialParentOfNode potenciální rodič uzlu node
 * @param node potenciální potomek uzlu potentialParentOfNode
 * @return true pokud je uzel potentialParentOfNode rodičem uzlu node, jinak false
 */
bool isNodeParent(Node *potentialParentOfNode, Node *node) {
    Node *tmpnode = node; // aktuální procházený uzel
    // od uzlu node postupujeme směrem vzhůrů a ověřujeme jestli jsme nanearazili na potentialParentOfNode
    while (!isNodeRoot(tmpnode)) {
        tmpnode = tmpnode->parent;
        if (tmpnode == potentialParentOfNode) {
            return true;
        }
    }
    if (tmpnode == potentialParentOfNode) {
        return true;
    }
    
    return false;
}

/**
 * Funkce vytvoří nový uzel, který se může dále začlenit do stromu
 * @param content obsah uzlu (symbol, který reprezentuje)
 * @param weight váha uzlu (frevence symbolu ve zprávě, pokud se jedná o listový uzel)
 * @param order pořadí uzlu ve stromě
 * @param parent rodič uzlu
 * @return ukazatel na nově vytvořený uzel
 */
Node * createNode(int content, u_int64_t weight, int order, Node *parent) {
    Node *node = new (std::nothrow) Node; // nově vytvořený uzel
    if (node != NULL) {
        node->content = content;
        node->rightChild = NULL;
        node->leftChild = NULL;
        node->parent = parent;
        node->weight = weight;
        node->order = order;
    }
    return node;
}

/**
 * Funkce vytvoří strom, tak že vytvoří uzel, který bude kořenem stromu
 * @return ukazatel na kořen stromu
 */
Node *createTree() {
    return createNode(NYT, 0, MAX_NODES, NULL);
}

/**
 * Funkce dealokuje paměť kterou zabírá strom, až na jeho listy, které ponechá
 * @param tree ukazatel na kořen huffmanova stromu
 */
void deleteTreeExceptLeafs(Node* tree) {
    // pokud jsme v listu neděláme nic (nedealokujem jej)
    if (isNodeLeaf(tree)) {
        return;
    }
    // jinak dealokujeme levý a pravý podstrom a nakonec samotný uzel
    else {
        deleteTreeExceptLeafs(tree->leftChild);
        deleteTreeExceptLeafs(tree->rightChild);
        delete tree;
    }
}

/**
 * Funkce najde a vrátí všechny listy v huffmanově stromu v pořadí zleva doprava
 * @param tree ukazatel na kořen huffmanova stromu
 * @return vektor ukazatelů na listy huffmnaova stromu
 */
vector<Node*> getAllLeafsInTree(Node* tree) {
    vector<Node*> vectorOfLeafs;
    // pokud jsme v listu uložíme do vektoru ukazatel na listový uzel
    if (isNodeLeaf(tree)) {
        vectorOfLeafs.push_back(tree);
    }
    // prohledáváme rekurzivně pod stromy
    else {
        // získáme vektor listů z levého podstromu
        vectorOfLeafs = getAllLeafsInTree(tree->leftChild);
        // získáme vektor listů z pravého podstromu
        vector<Node*> vectorOfLeafsTMP = getAllLeafsInTree(tree->rightChild);
        
        // zkonkatenujeme oba vektory
        vectorOfLeafs.insert(vectorOfLeafs.end(), vectorOfLeafsTMP.begin(), vectorOfLeafsTMP.end());
    }
    
    return vectorOfLeafs;
}

/**
 * Funkce provedete změnu měřítka (přeškálování) HUffmanova stromu, aby nedošlo k přetečení čítače váhy kořenového uzlu
 * @param tree ukazatel na kořen huffmanova stromu
 * @return ukazatel na kořen přeškálovaného HUffmanova stromu
 */
Node* scalingTree(Node* tree){
    // získáme listové uzly stromu
    vector<Node*> leafsInTree = getAllLeafsInTree(tree);
    
    // následně u těchto listů provedeme změnu měřítka jejich váhy, kdy jejich váhy podělíme 2 
    for (Node* currentLeaf : leafsInTree) {
        currentLeaf->weight = currentLeaf->weight / 2;
    }
    
    // následně smažeme (dealokujeme) všechny uzly huffmanova stromu, kromě listových u kterých jsme provedli změnu měřítka
    deleteTreeExceptLeafs(tree);
    
    // nakonec to hlavní: z listových uzlů, u kterých jsme změnili měřítko váhy opět vystavíme huffmanův strom zespoda nahoru 
    vector<Node*> nodesToConnect(leafsInTree); // průběžný seznam uzlů, které je třeba spojit do nadřazených uzlů
    
    Node *newTree; // nový přeškálovyný strom (ukazatel na kořenový uzel přeškálovanéhop stromu), který vrátíme
    
    // dokud nám nezbude redukcí/spojováním jeden uzel (kořen provádíme redukci uzlů na rodičovské uzly, tak že spojujeme vždy uzly s nejměnší vahou)
    while (nodesToConnect.size() != 1) {
        int indexToFirstNodeToConnect = 1; // index prvního uzlu, který se bude spojovat s druhým uzlem, inicializujeme na druhý uzel v seznamu (první uzel by měl mít stejnou nebo menší váhu než druhý uzel)
        int indexToSecondNodeToConnect = 0; // index druhého uzlu, který se bude spojovat s prvním uzlem, inicializujeme na první uzel v seznamu
        
        u_int64_t weightFirstNodeToConnect = (nodesToConnect[1])->weight; // váha prvního uzlu ke spojení, inicializujeme na váhu druhého uzlu v seznamu
        u_int64_t weightSecondNodeToConnect = (nodesToConnect[0])->weight; // váha druhého uzlu ke spojení, inicializujeme na váhu prvního uzlu v seznamu
        
        // procházíme průběžným seznamem uzlů ke spojení a hledáme dva uzly s nejnižší vahou, abychom je spojili
        for (int i = 2; i < nodesToConnect.size(); i++) {
            if ((nodesToConnect[i])->weight < indexToFirstNodeToConnect) {
                weightSecondNodeToConnect = weightFirstNodeToConnect;
                indexToSecondNodeToConnect = indexToFirstNodeToConnect;
                
                weightFirstNodeToConnect = (nodesToConnect[i])->weight;
                indexToFirstNodeToConnect = i;
            }
            
        }
       
        // pokud má první uzel větší váhu než druhý, prohodíme je
        if (indexToFirstNodeToConnect == weightSecondNodeToConnect) {
            int tmpIndexToFirstNodeToConnect = indexToFirstNodeToConnect;
            u_int64_t tmpWeightFirstNodeToConnect = weightFirstNodeToConnect;
            
            indexToFirstNodeToConnect = indexToSecondNodeToConnect;
            weightFirstNodeToConnect = weightSecondNodeToConnect;
            indexToSecondNodeToConnect = tmpIndexToFirstNodeToConnect;
            weightSecondNodeToConnect = tmpWeightFirstNodeToConnect;
        }
        
        // vytvoříme rodičovský uzel
        Node *parentNode = createNode(OLD_NYT, weightFirstNodeToConnect + weightSecondNodeToConnect, 0, NULL);
        if (parentNode == NULL) {
            return NULL;
        }
        
        parentNode->leftChild = nodesToConnect[indexToFirstNodeToConnect];
        parentNode->leftChild = nodesToConnect[indexToSecondNodeToConnect];
        
        // smažeme jeho děti ze seznamu uzlů určených ke spojení
        nodesToConnect.erase (nodesToConnect.begin()+indexToFirstNodeToConnect);
        nodesToConnect.erase (nodesToConnect.begin()+indexToSecondNodeToConnect - 1); 
        
        // rodičovský uzel vložíme do seznamu uzlů ke spojení
        nodesToConnect.push_back(parentNode);
    }
    newTree = nodesToConnect.front(); // získáme kořenový uzel
    
    // nakonec ještě přečíslujeme správně jednotlivé uzly, aby měly číslování vzestupně zleva doprava a zdola nahoru, tak jak zákon káže 
    renumberOrderOfAllNodesInTree(newTree);
    
    return newTree;
}

/**
 * Funkce nalezne a vrátí uzel, který obsahuje daný obsah (symbol), zároveň přes výstupní parametr vrátí cestu (huffmanův kód) k danému uzlu od kořenu
 * @param tree ukazatel na kořen huffmanova stromu
 * @param content hledaný obsah (symbol)
 * @param code výstupní parametr, pokud != NULL, potom do tohoto výstupního parametru uložíme kód (od ke kořenu k listu) hledaného symbolu v huffamnově stromu
 */
Node *searchNodeByContent(Node* tree, int content, deque<bool> *code) {
    // pokud jsme v listu vrátíme jej, pokud obsahuje hledaný obsah, jinak vrátíme NULL
    if (isNodeLeaf(tree)) {
        if (tree->content == content) {
            return tree;
        }
        else return NULL;
    }
    // prohledáváme rekurzivně pod stromy
    else {
        // prohledáme levý podstrom
        Node *node = searchNodeByContent(tree->leftChild, content, code);
        // pokud jsme našli hledaný uzel podle jeho obsahu, vrátíme jej a případně upravíme průběžný huffmanův kód
        if (node != NULL) {
            if (code != NULL) {
                (*code).push_front(0);
            }
            return node;
        }
        // pokud neobsahuje levý podstrom hledaný uzel, prohledáme pravý podstrom
        else {
            node = searchNodeByContent(tree->rightChild, content, code);
            // pokud jsme našli hledaný uzel podle jeho obsahu, vrátíme jej a případně upravíme průběžný huffmanův kód
            if (node != NULL) {
                if (code != NULL) {
                    (*code).push_front(1);
                }
                return node;
            }
        }
    }
    
}

/**
 * Funkce vykreslí (nebo spíše vypíše) strom (pro debuggování)
 * @param tree ukazatel na kořen huffmanova stromu
 * @param deep aktuální hloubka, ve které se nacházíme (0 = kořen, ...)
 */
void printTree(Node* tree, int deep) {
    
    string prefix = "";
    for (int i = 0; i < deep; i++) {
        prefix += "--";
    }
    if (tree->content == NYT) {
        printf("%s Pořadí: %d | Symbol: NYT | Váha: %d\n",prefix.c_str(), tree->order, (int) tree->weight);
    }
    else if (tree->content == OLD_NYT) {
        printf("%s Pořadí: %d | Symbol: OLD_NYT | Váha: %d\n",prefix.c_str(), tree->order, (int) tree->weight);
    }
    else {
        printf("%s Pořadí: %d | Symbol: %c | Váha: %d\n",prefix.c_str(), tree->order, tree->content, (int) tree->weight);
    }
    
    // pokud nejsme v listu, rekurzivně necháme vypsat levou a poté pravou větev uzlu
    if (!isNodeLeaf(tree)) {
        printTree(tree->leftChild, deep + 1);
        printTree(tree->rightChild, deep + 1);
    }
   
    
}

/**
 * Funkce vypíše obsah uzlu huffmanova stromu
 * @param node ukazatel na uzel
 */
void printNode(Node *node) {
    if (node->content == NYT) {
        printf("Pořadí: %d | Symbol: NYT | Váha: %d\n",node->order, (int) node->weight);
    }
    else if (node->content == OLD_NYT) {
        printf("Pořadí: %d | Symbol: OLD_NYT | Váha: %d\n", node->order, (int) node->weight);
    }
    else {
        printf("Pořadí: %d | Symbol: %c | Váha: %d\n",node->order, node->content, (int) node->weight);
    }
}

/**
 * Funkce která nalezne a vrátí nejvýznamější (s největším agtributem order) uzel stromu v rámci daného bloku (uzly se stejným atributem weight)
 * @param tree ukazatel na kořen stromu
 * @param weight váha uzlů v daném bloku
 * @return ukazatel na nejvýznamnější uzel (s největším agtributem order)  v rámci daného bloku uzlů o stejné váze
 */
Node *getMostSignificantNodeOfGroup(Node *tree, u_int64_t weight) {
    // pokud jsme v listu vrátíme jej, pokud jeho váha odpovídá váze bloku, jinak vracíme NULL
    if (isNodeLeaf(tree)) {
        if (tree->weight == weight) {
            return tree;
        }
        else return NULL;
    }
    // pokud se nejedná o list a je to uzel apotřící do daného bloku, vrátíme jej (jeho děti už nebudou v rámci bloku významější)
    else if (tree->weight == weight) {
        return tree;
    }
    // pokud se nejedná o uzel patřící do bloku, prohledáme jeho levý a pravý podstrom rekurzivně
    else {
        // prohledáme levý podstrom, aybchom v něm našli nejvýznamější uzel v dané skupině
        Node *mostSignificantNodeOfGroupFromLeftSubtree = getMostSignificantNodeOfGroup(tree->leftChild, weight);
        // prohledáme pravý podstrom, aybchom v něm našli nejvýznamější uzel v dané skupině
        Node *mostSignificantNodeOfGroupFromRightSubtree = getMostSignificantNodeOfGroup(tree->rightChild, weight);
        
        // v levém podstromě se uzel z dané skupiny nevyskytuje, v pravém podstromě se určitě vyskytovat bude (minimálně jeden uzel)
        if (mostSignificantNodeOfGroupFromLeftSubtree == NULL) {
            return mostSignificantNodeOfGroupFromRightSubtree;
        }
        // v pravém podstromě se uzel z dané skupiny nevyskytuje, v levém podstromě se určitě vyskytovat bude (minimálně jeden uzel)
        else if (mostSignificantNodeOfGroupFromRightSubtree == NULL) {
            return mostSignificantNodeOfGroupFromLeftSubtree;
        }
        // v levém i pravém podstromě se vyskytuje uzel z dané skupiny, musíme potrovnat který je významější (vyšší order)
        else {
            if (mostSignificantNodeOfGroupFromLeftSubtree->order > mostSignificantNodeOfGroupFromRightSubtree->order) {
                return mostSignificantNodeOfGroupFromLeftSubtree;
            }
            else {
                return mostSignificantNodeOfGroupFromRightSubtree;
            }
        }
    } 
}

/**
 * Funkce vrátí vektor obsahující struktury, kde každá struktura obsahuje uzel stromu a jeho hloubku
 * Jednotlivé uzly jsou seřazeny tak, že v rámci stejné hlozbky jsou ve vektoru seřazeny z leva do prava
 * Toho využijeme při přečíslování pořadí uzlů ve stromě pokud provedem swap uzlů
 * @param tree ukazatel na kořen huffmanova stromu
 * @param deep hloubka stromu ve které se pohybujeme
 * @return vektor obsahující struktury, kde každá struktura obsahuje uzel stromu a jeho hloubku
 */
vector<DeepOfNode> getListOfNodesAndTheirDeep(Node *tree, int deep) {
    vector<DeepOfNode> listOfNodesAndTheirDeep;
    // pokud jsme v listu uložíme do vektoru strukturu obsahující tento listový uzel a také jeho hloubku
    if (isNodeLeaf(tree)) {
        DeepOfNode deepOfNode;
        deepOfNode.deep = deep;
        deepOfNode.node = tree;
        listOfNodesAndTheirDeep.push_back(deepOfNode);
    }
    // prohledáváme rekurzivně pod stromy
    else {
        // získáme vektor uzlů z levého podstromu
        listOfNodesAndTheirDeep = getListOfNodesAndTheirDeep(tree->leftChild, deep + 1);
        // získáme vektor uzlů z pravého podstromu
        vector<DeepOfNode> listOfNodesAndTheirDeepTMP = getListOfNodesAndTheirDeep(tree->rightChild, deep + 1);
        
        // zkonkatenujeme oba vektory
        listOfNodesAndTheirDeep.insert(listOfNodesAndTheirDeep.end(), listOfNodesAndTheirDeepTMP.begin(), listOfNodesAndTheirDeepTMP.end());
        
        // pridáme současný uzel
        DeepOfNode deepOfCurrentNode;
        deepOfCurrentNode.deep = deep;
        deepOfCurrentNode.node = tree;
        listOfNodesAndTheirDeep.push_back(deepOfCurrentNode);
    }
    
    return listOfNodesAndTheirDeep;
}

/**
 * Funkce provede swap podstromů
 * @param tree ukazatel na kořen huffmanova stromu
 * @param leastSignificantNodeOfGroup ukazatel na nejméně významný uzel v rámci dané skupiny uzlů stejné váhy
 * @param mostSignificantNodeOfGroup ukazatel na nejvíce významný uzel v rámci dané skupiny uzlů stejné váhy
 */
void swapSubtrees(Node *tree, Node *leastSignificantNodeOfGroup, Node *mostSignificantNodeOfGroup) {

    // přímý rodič nejvýznamějšího uzlu v rámci dané skupiny uzlů stejné váhy
    Node *mostSignificantNodeOfGroupParent = mostSignificantNodeOfGroup->parent;
    // přímý rodič nejméně výzmaného uzlu v rámci dané skupiny uzlů stejné váhy
    Node *leastSignificantNodeOfGroupParent = leastSignificantNodeOfGroup->parent;
    
    // přehodíme uzly směrem výše (méně významný za více významný)
    if (mostSignificantNodeOfGroupParent->leftChild == mostSignificantNodeOfGroup) {
        mostSignificantNodeOfGroupParent->leftChild = leastSignificantNodeOfGroup;
    }
    else {
        mostSignificantNodeOfGroupParent->rightChild = leastSignificantNodeOfGroup;
    }
    leastSignificantNodeOfGroup->parent = mostSignificantNodeOfGroupParent;
    
    //přehodíme uzly směrem níže (více významný za méně významný)
    if (leastSignificantNodeOfGroupParent->leftChild == leastSignificantNodeOfGroup) {
        leastSignificantNodeOfGroupParent->leftChild = mostSignificantNodeOfGroup;
    }
    else {
        leastSignificantNodeOfGroupParent->rightChild = mostSignificantNodeOfGroup;
    }
    mostSignificantNodeOfGroup->parent = leastSignificantNodeOfGroupParent;
}

/**
 * Funkce provede přečíslování pořadí uzlů ve stromu 
 * @param tree ukazatel na kořen huffmanova stromu
 */
void renumberOrderOfAllNodesInTree(Node *tree) {
    // ziskame seznam struktur, kde každá obsahuje uzel a jeho hloubku, struktury jsou v rámci stejné hloubky seřazeny v seznamu zleva doprava
    vector<DeepOfNode> listOfNodesAndTheirDeep = getListOfNodesAndTheirDeep(tree, 0);
    
    int minOrderNumber = MAX_NODES - listOfNodesAndTheirDeep.size() + 1; // nejmnší hodnota atributu order u uzlu
    int maxDeep = 0; // maximální hloubka stromu
    vector<DeepOfNode>::iterator it;
    
    // nejdřív nalezenem maximální hloubku stromu
    for (it=listOfNodesAndTheirDeep.begin(); it<listOfNodesAndTheirDeep.end(); it++) {
        maxDeep = (((*it).deep) > maxDeep) ? (*it).deep : maxDeep;
    }
    
    // potom seřadíme uzly zleva doprava a zdola nahoru (změnou jejich atributu order)
    int currentOrderNumber = minOrderNumber; // hodnota která bude přiřazena atributu order příslušnému uzlu
    for (int i = maxDeep; i >= 0; i--) {
        for (it=listOfNodesAndTheirDeep.begin(); it<listOfNodesAndTheirDeep.end(); it++) {
            if ((*it).deep == i) {
                ((*it).node)->order = currentOrderNumber;
                currentOrderNumber++;
            }
        }
    }
}

/**
 * Funkce provede update stromu (přidá uzel na dané msíto, nebo upraví váhu daného uzlu a případně provede swap podstromů a přečíslování pořadí uzlů)
 * @param tree ukazatel na kořen huffmanova stromu
 * @param content symbol, který chceme do stromu uložit (buď nově a nebo zvýšíme jho frekvenci, pokud už ve stromě existuje)
 * @return -1 pokud se update stromu nepodaří, jinak 0
 */
int updateTree(Node *tree, int content) {

    // nalezneme uzel, který obsahuje daný symbol
    Node *node = searchNodeByContent(tree, content, NULL);

    // pokud se uzel nevyskytuje ve stromu, vytvoříme jej a přidáme jej za NYT uzel
    if (node == NULL) {
        // nalezenem NYT uzel
        Node *oldNYTnode = searchNodeByContent(tree, NYT, NULL);

        // vytvoříme uzel, který bude obsahovat dnaý smybol
        Node *newContentNode = createNode(content, 1, oldNYTnode->order - 1, oldNYTnode);
        // vytvoříme nový NYT uzel
        Node *newNYTnode = createNode(NYT, 0, oldNYTnode->order - 2, oldNYTnode);
        
        // alokace uzlu se nezdařila (málo paměti?)
        if (newContentNode == NULL || newNYTnode == NULL) {
            return AHEDFail;
        }
        
        // nový NYT uzel a nový uzel se symbolem přidáme za starý NYT uzel
        oldNYTnode->content = OLD_NYT;
        oldNYTnode->leftChild = newNYTnode;
        oldNYTnode->rightChild = newContentNode;
        oldNYTnode->weight += 1;
        
        // aktualizace váh rodičpvských uzlů a přitom kontrola jestli není třeba výměna podstromů ve stromě
        if (!isNodeRoot(oldNYTnode)) {
            Node *parentNode = oldNYTnode->parent; // aktuální rodičovský uzel
            while (!isNodeRoot(parentNode)) {
                // nalezenem nejvýznamejší uzel v dané skupině uzlů stejné váhy
                Node *mostSignificantNodeOfGroup = getMostSignificantNodeOfGroup(tree, parentNode->weight);
                
                // pokud je třeba provést swap podstromů, provedeme jej a přečíslujeme pořadí uzlů, aby byly seřazeny vzestupně zleva doprava a zdola nahoru 
                if (mostSignificantNodeOfGroup != parentNode && !isNodeParent(mostSignificantNodeOfGroup, parentNode)) {                 
                    swapSubtrees(tree, parentNode, mostSignificantNodeOfGroup);
                    renumberOrderOfAllNodesInTree(tree);
                }
                parentNode->weight += 1;
                parentNode = parentNode->parent; // posuneme se o úroveň výše
            }
            parentNode->weight += 1;
        }
    // pokud se uzel vyskytuje ve stromu, potom jej aktualizujeme (zvýšíme frekvenci výskytu symbolu)    
    }
    else {
        while (!isNodeRoot(node)) {
            // nalezenem nejvýznamejší uzel v dané skupině uzlů stejné váhy
            Node *mostSignificantNodeOfGroup = getMostSignificantNodeOfGroup(tree, node->weight);
            // pokud je třeba provést swap podstromů, provedeme jej a přečíslujeme pořadí uzlů, aby byly seřazeny vzestupně zleva doprava a zdola nahoru 
            if (mostSignificantNodeOfGroup != node && !isNodeParent(mostSignificantNodeOfGroup, node)) {
                swapSubtrees(tree, node, mostSignificantNodeOfGroup);
                renumberOrderOfAllNodesInTree(tree);
            }
            node->weight += 1;
            node = node->parent; // posuneme se o úroveň výše
        }
        node->weight += 1;
    }
    return AHEDOK;
}

/**
 * Funkce zakóduje znak jako posloupnost (deque) bitů, s tím že jejich počet je 9
 * 1. bit totiž je příznak že se nejedná o pseudo EOF, tedy první bit musí být 1
 * @param inputChar vstupní symbol (bytový), který se zakóduje
 * @return fronta bitů, do kterých je vstupní symbol zakódován
 */
deque<bool> codeCharacter(int inputChar) {
    deque<bool> codeOfCharacter;  // fronta bitů, do kterých je vstupní symbol zakódován 
    codeOfCharacter.push_back(1); // nejdřív vložíme bit 1, což znamneá že daný symbol není pseudo EOF
    // získáme bity ze kterých se skládá vstupní symbol pomocí bitových operací
    for (int i = 0;i < 8; i++) {
        if (((((unsigned char) 128) >> i) & ((unsigned char) inputChar)) == 0) {
            codeOfCharacter.push_back(0);
        }
        else {
            codeOfCharacter.push_back(1);
        }
    }
    
    return codeOfCharacter;
}

/**
 * Funkce je volána při dekódování kdy z přečteného bytu chceme získat posloupnost 0 a 1
 * Je docela podobná funkci codeCharacter, která se používá při kódování znaku na posloupnost bitů
 * @param inputChar vstupní symbol (bytový), ze kterého chceme získat posloupnost bitů
 * @return fronta bitů, ze kterých se vstupní symbol skládá
 */
deque<bool> getBitsSequenceFromByte(int inputByte) {
    deque<bool> sequenceOfbits; // fronta bitů, ze kterých se vstupní symbol skládá
    // získáme bity ze kterých se skládá vstupní symbol pomocí bitových operací
    for (int i = 0;i < 8; i++) {
        if (((((unsigned char) 128) >> i) & ((unsigned char) inputByte)) == 0) {
            sequenceOfbits.push_back(0);
        }
        else {
            sequenceOfbits.push_back(1);
        }
    }
    
    return sequenceOfbits;
}

/**
 * Funkce z posloupnosti bitů získá jeden bajt (z pprvních 8 bitů), který je možné zapsat do souboru
 * @param code fronta bitů (musí obsahovat alespoň 8 prvků/bitů)
 * @return bajt, který odpovídá dané posloupnosti bitů
 */
unsigned char getEncodedByteFromDequeOfBits(deque<bool> code) {
    int byte = 0; // inicializace bajtu
    
    // převedeme binární kód do desítkové soustavy a tím získáme bajt
    for (int i = 7; i >= 0; i--) {
        if (code[7 - i] == 1) {
            byte += pow(2, i);
        }
    }

    return (unsigned char) byte;
}

/**
 * Funkce spojí dva datové kontejnery Deque/fronta obousměrná do jednoho (prvního)
 * @param firstDeque první fronta, ke které přidáme prvky z druhé fronty a tím obě fronty spojíme, jedná se o výstupní parametr
 * @param secondDeque druhá fronta
 */
template <class T>
void mergeDeques(deque<T> *firstDeque, deque<T> *secondDeque) {
    for (T item : *secondDeque) { 
        (*firstDeque).push_back(item);
    }
}

/* Nazev:
 *   AHEDEncoding
 * Cinnost:
 *   Funkce koduje vstupni soubor do vystupniho souboru a porizuje zaznam o kodovani.
 * Parametry:
 *   ahed - zaznam o kodovani
 *   inputFile - vstupni soubor (nekodovany)
 *   outputFile - vystupni soubor (kodovany)
 * Navratova hodnota: 
 *    0 - kodovani probehlo v poradku
 *    -1 - pri kodovani nastala chyba
 */
int AHEDEncoding(tAHED *ahed, FILE *inputFile, FILE *outputFile)
{
    Node *tree = createTree(); // kořenový uzel
    if (tree == NULL) {
        return AHEDFail; 
    }

    int inputChar;           // načtený bajt/znak/symbol
    deque<bool> code;        // průběžný kód (posloupnost bitů) k zapsání na výstup
    deque<bool> currentCode; // kód načteného symbolu v huffmanově stromě
    deque<bool> eofCode {0, 0, 0, 0, 0, 0, 0, 0, 0}; // kód eof symbolu 

    bool firstIteration = true; // příznak první iterace cyklu 
    // načítám symboly a kóduji je na posloupnosti bitů, které po bytech zapisuji na výstup
    do {
        // pokud váha kořenového uzlu dosáhla maximální možné hodnoty, provedeme přeškálování (změnu měřítka) huffmanova stromu
        if (tree->weight == numeric_limits<uint64_t>::max()) {
            tree = scalingTree(tree);
            // nastala chyba při přeškálování stromu
            if (tree == NULL) {
                return AHEDFail; 
            }
        }
        currentCode.clear(); // nezapomeneme vždy na začátku cyklu vyčistit aktuální kód symbolu v huffmanově stromě
        inputChar = fgetc(inputFile);     // načteme bajt ze vstupu
        ahed->uncodedSize += (int64_t) 1; // zvýšíme velikost vstupního souboru
        // pokud při čtení došlo k chybě
        if (ferror (inputFile)) {
            fprintf (stderr, "\nChyba při čtení ze vstupního souboru\n");
            return AHEDFail;
        }
        
        // pokud je soubor prázdný, tak do něj nic nedáváme
        if (firstIteration) {
            if (inputChar == EOF) {
                break;
            }
            firstIteration = false;
        }

        // pokusíme se najít symbol v huffmanově stromě a pokud existuje, zjistíme i jeho huffmanův kód, 
        // který přidáme k průběžnému kódu, určenému k zápisu do souboru
        Node *node = searchNodeByContent(tree, inputChar, &currentCode);
        mergeDeques<bool>(&code, &currentCode);

        // pokud se daný symbol nenachází ve stromu
        if (node == NULL) {

            // nalezneme ve stromu uzel NYT a jeho hufmanův kód přidáme k průběžnému kódu, určenému k zápisu do souboru
            searchNodeByContent(tree, NYT, &currentCode);
            mergeDeques<bool>(&code, &currentCode);

            // pokud jsme dočetli do konce souboru přidáme k průběžnému kódu, určenému k zápisu do souboru kód pro pseudo EOF
            if (inputChar == EOF) {
                mergeDeques<bool>(&code, &eofCode);
            }
            // jinak přidáme průběžnému kódu, určenému k zápisu do souboru kód pro symbol, který zatím není v huffmanově stromě
            else {
                deque<bool> codedCharacter = codeCharacter(inputChar);
                mergeDeques<bool>(&code, &codedCharacter);
            }
        }
        
        // pokud jsme dočetli do konce vstupu a nemáme počet bitů, který je násobkem 8, 
        // tak je všechny nejsme schopni zapsat jako bajty a některé nám zbudou, takže je třeba provést padding bitů 0
        if (inputChar == EOF && code.size() % 8 != 0) {
            while (code.size() % 8 != 0) {
                code.push_back(0);
            }
        }

        // dokud máme k zapsání 8 bitů, zapíšeme je jako bajt
        while (code.size() >= 8) {
            // převedeme prvních 8 bitů, z průběžných bitů určených k zapsání na výstup, na bajt
            unsigned char encodedByte = getEncodedByteFromDequeOfBits(code);
            code.erase (code.begin(),code.begin()+8); // smažeme prvních 8 bitů (ze kterých jsme teď vytvořili byte=unsigned char), z fronty bitů k zapsání
            
            fputc(encodedByte, outputFile); // bajt zapíšeme na výstup
            ahed->codedSize += (int64_t) 1; // zvýšíme velikost výstupního souboru
            
            // pokud při zápisu došlo k chybě
            if (ferror (outputFile)) {
                fprintf (stderr, "\nChyba při zápisu do výstupního souboru\n");
                return AHEDFail;
            }
        }

        // provedeme update stromu
        if (updateTree(tree, inputChar) != AHEDOK) {
            return AHEDFail;
        }

    } while (inputChar != EOF);
    
    return AHEDOK;
}

/* Nazev:
 *   AHEDDecoding
 * Cinnost:
 *   Funkce dekoduje vstupni soubor do vystupniho souboru a porizuje zaznam o dekodovani.
 * Parametry:
 *   ahed - zaznam o dekodovani
 *   inputFile - vstupni soubor (kodovany)
 *   outputFile - vystupni soubor (nekodovany)
 * Navratova hodnota: 
 *    0 - dekodovani probehlo v poradku
 *    -1 - pri dekodovani nastala chyba
 */
int AHEDDecoding(tAHED *ahed, FILE *inputFile, FILE *outputFile)
{
    Node *tree = createTree(); // kořenový uzel
    if (tree == NULL) {
        return AHEDFail; 
    }

    int inputByte;              // načtený bajt vstupního souboru
    unsigned char decodedChar;  // dekódovaný znak ze vstupního souboru
    deque<bool> unprocessedBitSequence; // průběžně načítaný binární kód (posloupnost/deque obsahující 0 a 1), který e určen ke zpracování

    bool firstIteration = true;        // příznak první iterace cyklu 
    bool readByteFromInputFile = true; // příznak, že je třeba načíst další byte ze vstupního souboru aby mohl pokračovat proces dekódování
    
    DecodeStates currentState = READ_9_BIT_CHARACTER; // aktuální stav v rpocesu dekódování je takový že chceme načíst 9bitový kód znaku
    Node *currentNode = tree; // aktuální uzel ve kterém jsme nacházíme při vyhodnocování huffmanova binárního kódu
    // načítám bajty ze vstupu, vytvářím řezetce bitů, dekóduji je na symboly a zapisuji je na výstup
    do {
        // pokud váha kořenového uzlu dosáhla maximální možné hodnoty, provedeme přeškálování (změnu měřítka) huffmanova stromu
        if (tree->weight == numeric_limits<uint64_t>::max()) {
            tree = scalingTree(tree);
            // nastala chyba při přeškálování stromu
            if (tree == NULL) {
                return AHEDFail; 
            }
        }
        
        // pokud je potřeba načíst další bajt zre vstupu, aby mohlo pokračovat dekódování
        if (readByteFromInputFile) {

            inputByte = fgetc(inputFile); // načteme bajt
            // získáme posloupnost bitů z bajtu a přidáme ji do posloupnisti dosud načtených ale nezpracovaných bitů
            deque<bool> bitsSequenceFromInputByte = getBitsSequenceFromByte(inputByte);
            mergeDeques<bool>(&unprocessedBitSequence, &bitsSequenceFromInputByte);
            
            ahed->codedSize += (int64_t) 1; // zvýšíme velikost vstupního souboru          
            
            // pokud při čtení došlo k chybě
            if (ferror (inputFile)) {
                fprintf (stderr, "\nChyba při čtení ze vstupního souboru\n");
                return AHEDFail;
            }
            
            readByteFromInputFile = false; // kontrolujeme kolik toho přečteme, implicitně nečteme nic, dokud to skutečně není potřeba, šetříme tak RAM
        }
        
        // pokud je vstupní soubor prázdný, tak do výstupního nic nedáváme
        if (firstIteration) {
            if (inputByte == EOF) {
                break;
            }
            firstIteration = false;
        }
        
        // podle současného stavu se rozhodnem o akci kterou provedeme
        switch (currentState) {
            // je třeba načíst 9bitový znak a dekódovat jej na původní 8bitový znak
            case READ_9_BIT_CHARACTER:
                // abychom mohli přečíst zakódovaný 9bitový znak, musíme mít k dispozici alespoň těchto 9bitů, takže skočíme na další iteraci cyklu ve klter=é načteme další znak
                if (unprocessedBitSequence.size() < 9) {
                    readByteFromInputFile = true;
                    continue;
                }
                
                // pokud je prvním bitem z 9bitového zakódovaného znaku 0, tak se jedná o znak pseudo EOF a značí konec souboru
                // nastavíme inputByte na EOF, což ukončí proces dekódování
                if (unprocessedBitSequence[0] == 0) {
                    inputByte = EOF;
                    break;
                }
                
                // smažeme první bit, nyní víme že je to bit 1 a značí že se jedná o normální znak, nikoliv pseudo EOF
                unprocessedBitSequence.erase (unprocessedBitSequence.begin(),unprocessedBitSequence.begin()+1);
                // získáme dekódovaný znak z prvních 8 bitů nezpracované sekvence bitů
                decodedChar = getEncodedByteFromDequeOfBits(unprocessedBitSequence);

                // těchto použitých 8 biů smažeme
                unprocessedBitSequence.erase (unprocessedBitSequence.begin(),unprocessedBitSequence.begin()+8); 
                // dále zapíšeme dekóovaný znak na výstup
                currentState = WRITE_DECODED_CHAR_TO_OUTPUT;
                
                break;
            
                // je třeba zapsat dekódovaný znak na výstup    
            case WRITE_DECODED_CHAR_TO_OUTPUT:
                fputc((int) decodedChar, outputFile); // zapíšeme dekódovaný znak na výstup
                ahed->uncodedSize += (int64_t) 1;     // zvýšíme velikost výstupního souboru
            
                // pokud při zápisu došlo k chybě
                if (ferror (outputFile)) {
                    fprintf (stderr, "\nChyba při zápisu do výstupního souboru\n");
                    return AHEDFail;
                }

                // provedeme update stromu
                if (updateTree(tree, decodedChar) != AHEDOK) {
                    return AHEDFail;
                }
                
                // dále budeme další bity v dosud nezpracované načtené sekvenci bitů považovat za huffmanův kód symbolu v huffmanově stromě
                currentState = READ_HUFFMAN_CODE;
                break;
            
            // čteme nezpracovanou sekvenci bitů a vyhodnocujeme jakému znaku v huffmanově stromě patří
            case READ_HUFFMAN_CODE:
                // čteme bity a vyhodnocujeme je, dokud nenarazíme na list (bude obsahovat daný symbol) nebo dokud máme takové bity ke zpracování, 
                // pokud je nemáme ke zpracování je třeba načíst další bajt ze vstupu, abychom tyto bity z daného bajtu vytáhli
                while (!isNodeLeaf(currentNode) && unprocessedBitSequence.size() > 0) {
                    // 0 bit znamneá posun v huffmanově stromě směrem k levému potomku
                    if (unprocessedBitSequence[0] == 0) {
                        currentNode = currentNode->leftChild;
                    }
                    // 1 bit znamneá posun v huffmanově stromě směrem k pravému potomku
                    else {
                        currentNode = currentNode->rightChild;
                    }
                    // zpracovaný bit odstraníme z fronty bitů ke zpracování
                    unprocessedBitSequence.erase(unprocessedBitSequence.begin(),unprocessedBitSequence.begin()+1);
                }
                // pokud jsme přestali zpracovávat bity, prootže je už nemáme, načteme si další ze souboru
                if (unprocessedBitSequence.size() == 0) {
                    readByteFromInputFile = true;
                }
                // pokud jsme přestali zpracovávat bity, protože jsme došli až k listovému uzlu
                if (isNodeLeaf(currentNode)) {
                    // listový uzel je NYT, tzn, že ve vstupním souboru/ načtené posloupnosti bitů je dále uložen 9bitový kód znaku, ktrý dále přečteme.
                    if (currentNode->content == NYT) {
                        currentState = READ_9_BIT_CHARACTER;
                    }
                    // listový uzel je znak, zapíšeme jej do výstupního souboru
                    else {
                        decodedChar = (unsigned char) currentNode->content;
                        currentState = WRITE_DECODED_CHAR_TO_OUTPUT;
                    }
                    currentNode = tree;
                }
                break;
        }
        
    } while (inputByte != EOF);
    
    return AHEDOK;
}



