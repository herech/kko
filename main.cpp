/*
 * Autor: Jan Herec (xherec00)
 * Datum: 11. února 2017
 * Soubor: main.cpp
 * Komentar: Aplikace, která provádí (s využitím knihovních funkcí) Adaptivní huffmanovo de(kódování) nad 8bitovými symboly.
 *           Vstupem může být soubor nebo stdin, stejně tak výstupem může být soubor nebo stdout.
 * Kodovani: UTF-8
 */ 

#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include "ahed.hpp"
#include <getopt.h>
#include <iostream>
#include <fstream>

using namespace std;

// ##################### DEFINICE DATOVÝCH STRUKTUR ############################

/**
 * Možné akce programu 
 */
enum Actions { Encode,Decode,NoAction};

/* Struktura, ktera obsahuje zpracovane vstupní parametry programu */
typedef struct{
	Actions actionWithInput;   // akce programu	
        FILE *inputFile;           // otevřený vstupní soubor
        FILE *outputFile;          // otevřený výstupní soubor
        string logFileName;        // název souboru výstupní zprávy
        bool printReportToLogFile; // příznak jestli se má vypsat výstupní zpráva do určeného souboru
} tProcessedParameters;

// ########################## PROTOTYPY FUNKCÍ #################################

/**
 * Funkce pro výpis nápovědy (a informací o programu)
 */
void printHelp();

/**
 * Funkce pro zpracování vstupních parametrů 
 * @param argc počet vstupních argumentů
 * @param argv vstupní argumenty
 * @param processedParameters výstupní parametr, který představuje strukturu se zpracovanými parametry
 */
void processTheParams(int argc, char **argv, tProcessedParameters* processedParameters);

/**
 * Klasicka funkce main, ktera realizuje cely program volanim podprogramu
 * @param argc počet vstupních argumentů
 * @param argv pole vstupních argumentů
 * @return -1 při chybě, jinak 0, pokud aplikace skončila standardně
 */
int main(int argc, char **argv);

// ######################### DEFINICE FUNKCÍ ###################################

/**
 * Funkce pro výpis nápovědy (a informací o programu)
 */
void printHelp() {
    cout <<
    "\n\t --- PROGRAM AHED --- \n\n"

    "Popis:\n"
        "\tProgram ahed zajišťuje adaptivní kódování a dekódování nad 8bitovými symboly\n\n"

    "Použití\n"
        "\t./ahed  {-c | -x} [-i ifile] [-o ofile] [-l logfile] [-h] \n\n"

    "Parametry:\n"
        "\t{-c | -x}"
            " - Povinně je uveden buď parametr -c (vyjadřuje akci kódování vstupu), nebo parametr -x (vyjadřuje akci dekódování vstupu).\n\n"

        "\t[-i ifile]"
            " - Volitelný parametr -i s hodnotou ifile specifikuje název vstupního souboru. Pokud nebude zadán, tak se bude za vstupní soubor považovat standardní vstup.\n\n"

        "\t[-o ofile]"
            " - Volitelný parametr -o s hodnotou ofile specifikuje název výstupního souboru. Pokud nebude zadán, tak se bude za výstupní soubor považovat standardní výstup.\n\n"

        "\t[-l logfile]"
            " - Volitelný parametr -l s hodnotou logfile specifikuje název souboru výstupní zprávy. Pokud nebude zadán, tak výpis zprávy bude ignorován.\n\n"

        "\t[-h]"
            " - Volitelný parametr -h znamená, že se vypíše nápověda a program se ukončí\n\n";
}

/**
 * Funkce pro zpracování vstupních parametrů 
 * @param argc počet vstupních argumentů
 * @param argv vstupní argumenty
 * @param processedParameters výstupní parametr, který představuje strukturu se zpracovanými parametry
 */
void processTheParams(int argc, char **argv, tProcessedParameters* processedParameters) {
    string inputFileName = "";    // nazev vstupniho souboru 
    bool isInputFromStdin = true; // příznak jestli je vstupní soubor standartní vstup
    
    string outputFileName = "";   // nazev výstupního souboru 
    bool isOutputToStdout = true; // příznak jestli je výstupní soubor standartní vstup
    
    string logFileName = "";           // nazev souboru výstupní zprávy
    bool printReportToLogFile = false; // příznak jestli se má vypsat výstupní zpráva do určeného souboru
    
    Actions actionWithInput = NoAction; // jaká akce se má provádět nad vstupem
    
    int param;  // aktuální načtený vstupní parametr
    opterr = 0; // vypneme výchozí výpis chyb v souvoslosti s parsování vtupních parametrů
    while ((param = getopt(argc, argv,"i:o:l:cxh")) != -1) {
        switch (param) {
            // parametr -i pro název vstupního souboru
            case 'i' : 
                inputFileName = string(optarg);
                isInputFromStdin = false;
                break;
            // parametr -o pro název výstupního souboru
            case 'o' : 
                outputFileName = string(optarg);
                isOutputToStdout = false;
                break;
            // parametr -l pro název souboru výstupní zprávy
            case 'l' : 
                logFileName = string(optarg);
                printReportToLogFile = true;
                break;
            // parametr -c určující akci kódování
            case 'c' : 
                // pokud byla zadána již jiná akce, tak je to chyba, program může provést jen jednu akci
                if (actionWithInput != NoAction) {
                    fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Bylo zadáno více akcí! \nPřečtěte si prosím pečlivě nápovědu:\n");
                    printHelp();
                    exit(EXIT_FAILURE);
                }
                actionWithInput = Encode;
                break;
            // parametr -x určující akci dekódování
            case 'x' : 
                // pokud byla zadána již jiná akce, tak je to chyba, program může provést jen jednu akci
                if (actionWithInput != NoAction) {
                    fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Bylo zadáno více akcí! \nPřečtěte si prosím pečlivě nápovědu:\n");
                    printHelp();
                    exit(EXIT_FAILURE);
                }
                actionWithInput = Decode;
                break;
            // parametr -c pro výpis nápovědy a ukončení programu
            case 'h' : 
                printHelp();
                exit(EXIT_SUCCESS);
                break;
            // při chybě
            default: 
                fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů!\nPřečtěte si prosím pečlivě nápovědu:\n");
                printHelp();
                exit(EXIT_FAILURE);
        }
    }
    
    // pokud nebyla zadána žádná akce, je to chyba, něco se vstupem přece dělat musíme!
    if (actionWithInput == NoAction) {
        fprintf(stderr, "\nChyba: Chyba při zpracování vstupních parametrů! Nebyla zadána žádná akce!\nPřečtěte si prosím pečlivě nápovědu:\n");
        printHelp();
        exit(EXIT_FAILURE);
    }
    
    FILE *inputFile;  // vstupní soubor
    FILE *outputFile; // výstupní soubor
    
    // připravíme vstupní soubor, než jej pošleme jako parametr knihovní funkci
    if (isInputFromStdin) {
        inputFile = stdin;
    }
    else {
        inputFile = fopen (inputFileName.c_str() , "r");
        if (inputFile == NULL) {
            perror ("Chyba při otevírání vstupního souboru.");
            exit(EXIT_FAILURE);
        }
    }
    
    // připravíme výstupní soubor, než jej pošleme jako parametr knihovní funkci
    if (isOutputToStdout) {
        outputFile = stdout;
    }
    else {
        outputFile = fopen (outputFileName.c_str() , "w");
        if (outputFile == NULL) {
            perror ("Chyba při otevírání výstupního souboru.");
            exit(EXIT_FAILURE);
        }
    }
    
    // nastavíme parametry ve struktuře, která je výstupním parametrem
    processedParameters->actionWithInput = actionWithInput;	
    processedParameters->inputFile = inputFile; 
    processedParameters->outputFile = outputFile;
    processedParameters->logFileName = logFileName;
    processedParameters->printReportToLogFile = printReportToLogFile;
}

/**
 * Klasicka funkce main, ktera realizuje cely program volanim podprogramu
 * @param argc počet vstupních argumentů
 * @param argv pole vstupních argumentů
 * @return -1 při chybě, jinak 0, pokud aplikace skončila standardně
 */
int main(int argc, char **argv)
{
    // zpracujeme vstupní parametry
    tProcessedParameters processedParameters;
    processTheParams(argc, argv, &processedParameters);
    
    // provedeme danou akci (kodovani nebo dekodovani vstupu) skrze volani knihovních funkcí
    tAHED tahed; 
    tahed.codedSize = 0;
    tahed.uncodedSize = 0;
    int result;
    if (processedParameters.actionWithInput == Encode) {
        result = AHEDEncoding(&tahed, processedParameters.inputFile, processedParameters.outputFile);
    }
    else if (processedParameters.actionWithInput == Decode) {
        result = AHEDDecoding(&tahed, processedParameters.inputFile, processedParameters.outputFile);
    }
    
    // zavřeme otevřené soubory
    if (processedParameters.inputFile != NULL) fclose (processedParameters.inputFile);
    if (processedParameters.outputFile != NULL) fclose (processedParameters.outputFile);
    
    // akce (kodovani/dekodovani) probehla v pořádku a je zde od uzivatele požadavek na vypsání výstupní zprávy do souboru
    if (result == 0 && processedParameters.printReportToLogFile) {
        
        ofstream logFile (processedParameters.logFileName.c_str());
        if (logFile.is_open())
        {
            logFile << "login = xherec00\n";
            logFile << "uncodedSize = " << tahed.uncodedSize << "\n";
            logFile << "codedSize = " << tahed.codedSize << "\n";
            if (logFile.bad()) {
                perror ("Chyba při otevírání souboru výstupní zprávy.");
                logFile.close();
                exit(EXIT_FAILURE);
            }
          
            logFile.close();
        }
        else {
            perror ("Chyba při otevírání souboru výstupní zprávy.");
            exit(EXIT_FAILURE);
        }
    }
    // chyba během kodovani nebo dekodovani
    else if (result != 0){
        fprintf(stderr, "\nChyba: Chyba během (de)kodovani!\n");
        return result;
    }
    
    return 0;
            
}


	
